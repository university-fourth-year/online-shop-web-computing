# online-shop-web-computing
## Name
Kebab shop

## Description
Development of an online kebab shop with JDBC, where you can register, sign-in, explore the shop, buy, etc.

## Authors and acknowledgment
Adrián F. Brenes Martínez & Tomás Sánchez de Dios.

## Project status
The project is being developed and has not been graded jet. For this reason, it will not be uploaded until the 13th of May of 2022, date when it has to be handed to the professors. If you want to have access to the project, please, contact me at adrianbrenes2000@protonmail.com. 
